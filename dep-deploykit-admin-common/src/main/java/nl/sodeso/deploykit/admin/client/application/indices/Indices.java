package nl.sodeso.deploykit.admin.client.application.indices;

import nl.sodeso.deploykit.admin.client.application.indices.actions.*;
import nl.sodeso.gwt.ui.client.controllers.center.AbstractCenterPanel;
import nl.sodeso.gwt.ui.client.controllers.notification.Notification;
import nl.sodeso.gwt.ui.client.controllers.notification.NotificationController;
import nl.sodeso.gwt.ui.client.controllers.notification.events.CreateNotificationEvent;
import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithDocumentation;
import nl.sodeso.gwt.ui.client.form.EntryWithSingleWidget;
import nl.sodeso.gwt.ui.client.form.button.NoButton;
import nl.sodeso.gwt.ui.client.form.button.SimpleButton;
import nl.sodeso.gwt.ui.client.form.button.YesButton;
import nl.sodeso.gwt.ui.client.form.input.TextAreaField;
import nl.sodeso.gwt.ui.client.form.input.TextAreaToolsField;
import nl.sodeso.gwt.ui.client.panel.LegendPanel;
import nl.sodeso.gwt.ui.client.panel.PopupWindowPanel;
import nl.sodeso.gwt.ui.client.panel.WindowPanel;
import nl.sodeso.gwt.ui.client.trigger.Trigger;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.gwt.ui.client.util.Align;
import nl.sodeso.gwt.websocket.client.WebSocketClientController;
import nl.sodeso.gwt.websocket.client.event.WebSocketRecievedEvent;
import nl.sodeso.gwt.websocket.client.event.WebSocketRecievedEventHandler;

/**
 * @author Ronald Mathies
 */
public class Indices extends AbstractCenterPanel implements WebSocketRecievedEventHandler { //

    private StringType console = new StringType();

    private ProgressBar progressBar = null;

    private SimpleButton rebuildIndicesButton;
    private SimpleButton updateIndicesButton;
    private SimpleButton checkIndicesButton;

    private WindowPanel windowPanel = null;

    public Indices() {
        super();

        setFullHeight(true);
        WebSocketClientController.instance().addWebSocketReceivedEventHandler(this);
    }

    @Override
    public void beforeAdd(Trigger trigger) {
        if (windowPanel != null) {
            WebSocketClientController.instance().send(new ResendLastIndicesMessage());
            trigger.fire();

            return;
        }

        windowPanel = new WindowPanel("indices-window", "Indices", WindowPanel.Style.INFO);

        EntryForm entryForm = new EntryForm("indices-form");
        entryForm.addEntry(new EntryWithDocumentation("indices-doc",
                "The indices need to be updated when the data structure of the application has changed (see the release notes for information about this). There are two actions that can be performed from this screen:<br/>" +
                        "<ul>" +
                        "<li>Check Indices will check if the indices are in sync with the database, when this is not the case you can perform the update action, performing this action does not affect the performance or functionality of the application in any way.</li>" +
                        "<li>Update Indices will synchronize the indices with the database, any abnormalities will be synchronised. The process of updating the indices will take a while depending on the amount of data." +
                        "<li>Rebuilding Indices will delete all existing indices and rebuild them completely. The process of rebuilding the indices could take a long time depending on the amount of data, any user activity in that mean time should not be allowed since it might interfere with the synchronization." +
                        "</ul>", Align.LEFT));

        LegendPanel legendPanel = new LegendPanel(null, "Check / Update Indices");
        legendPanel.add(entryForm);
        windowPanel.addToBody(legendPanel);

        EntryForm consoleEntryForm = new EntryForm("console-form");

        consoleEntryForm.addEntry(new EntryWithDocumentation("console-doc",
                "The console will display the progress when the indices are being checked or updated, when the process is started you can continue to use this application and come back later to check on the progress. "));

        progressBar = new ProgressBar();
        progressBar.setProgress(0, 100);
        consoleEntryForm.addEntry(new EntryWithSingleWidget("", progressBar));

        TextAreaField consoleField = new TextAreaField("console", console);
        consoleField.setVisibleLines(6);
        consoleField.setEnabled(false);
        TextAreaToolsField toolsField = new TextAreaToolsField("Console", consoleField);
        toolsField.addZoomOption();
        consoleEntryForm.addEntry(new EntryWithSingleWidget(null, toolsField));
        LegendPanel consoleLegendPanel = new LegendPanel(null, "Console");
        consoleLegendPanel.add(consoleEntryForm);
        windowPanel.addToBody(consoleLegendPanel);

        rebuildIndicesButton = new SimpleButton("indices-rebuild", "Rebuild Indices", SimpleButton.Style.RED);
        rebuildIndicesButton.addClickHandler((event) -> rebuildIndices());
        updateIndicesButton = new SimpleButton("indices-update", "Update Indices", SimpleButton.Style.ORANGE);
        updateIndicesButton.addClickHandler((event) -> updateIndices());
        checkIndicesButton = new SimpleButton("indices-check", "Check Indices", SimpleButton.Style.BLUE);
        checkIndicesButton.addClickHandler((event) -> checkIndices());

        windowPanel.addToFooter(Align.RIGHT, rebuildIndicesButton, updateIndicesButton, checkIndicesButton);
        add(windowPanel);

        WebSocketClientController.instance().send(new ResendLastIndicesMessage());

        trigger.fire();
    }

    public void onEvent(WebSocketRecievedEvent event) {
        if (event.getMessage() instanceof IndicesActionStartedMessage) {
            IndicesActionStartedMessage message = (IndicesActionStartedMessage)event.getMessage();
            console.setValue(message.getMessage());
        } else if (event.getMessage() instanceof IndicesActionProgressMessage) {
            IndicesActionProgressMessage message = (IndicesActionProgressMessage)event.getMessage();
            console.setValue(console.getValue() + "\n" + message.getMessage());
            progressBar.setProgress(message.getPercentageCompleted(), 100);
        } else if (event.getMessage() instanceof IndicesActionCompletedMessage) {
            IndicesActionCompletedMessage message = (IndicesActionCompletedMessage)event.getMessage();

            CreateNotificationEvent notification =
                    new CreateNotificationEvent(Notification.Style.INFO, "Indices Progress", message.getMessage(), true);
            NotificationController.instance().getEventBus().fireEvent(notification);

            console.setValue(console.getValue() + "\n" + message.getMessage());
            progressBar.setProgress(100, 100);
        }

        boolean enable = event.getMessage() instanceof IndicesActionCompletedMessage;
        rebuildIndicesButton.setEnabled(enable);
        updateIndicesButton.setEnabled(enable);
        checkIndicesButton.setEnabled(enable);
    }

    private void checkIndices() {
        WebSocketClientController.instance().send(new StartCheckIndicesMessage());
    }

    private void updateIndices() {
        PopupWindowPanel popupWindowPanel = new PopupWindowPanel("indices-update-popup", "Updating indices", WindowPanel.Style.ALERT);
        EntryForm entryForm = new EntryForm(null);
        entryForm.addEntry(new EntryWithDocumentation(null, "Updating indices might take some time, if any users are actively searching then this might lead to unexpected results. Are you sure you want to continue?"));
        popupWindowPanel.addToBody(entryForm);

        YesButton.WithLabel yesButton = new YesButton.WithLabel((event) -> {
            popupWindowPanel.close();
            WebSocketClientController.instance().send(new StartUpdateIndicesMessage());
        });

        popupWindowPanel.addToFooter(Align.RIGHT,
            new NoButton.WithLabel((event) -> popupWindowPanel.close()),
            yesButton);
        popupWindowPanel.center(true, true);
    }

    private void rebuildIndices() {
        PopupWindowPanel popupWindowPanel = new PopupWindowPanel("indices-rebuild-popup", "Rebuilding indices", WindowPanel.Style.ALERT);
        EntryForm entryForm = new EntryForm(null)
            .addEntry(new EntryWithDocumentation(null, "Rebuilding indices could take a very long time depending on the amount of data. Any user activity in that mean time should not be allowed since it might interfere with the synchronization. Are you sure you want to continue?"));
        popupWindowPanel.addToBody(entryForm);

        YesButton.WithLabel yesButton = new YesButton.WithLabel((event) -> {
            popupWindowPanel.close();
            WebSocketClientController.instance().send(new StartBuildIndicesMessage());
        });

        popupWindowPanel.addToFooter(Align.RIGHT,
            new NoButton.WithLabel((event) -> popupWindowPanel.close()),
            yesButton);
        popupWindowPanel.center(true, true);
    }

}
