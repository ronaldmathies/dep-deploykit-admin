package nl.sodeso.deploykit.admin.client.application.indices.actions;

import nl.sodeso.gwt.websocket.client.Message;

/**
 * @author Ronald Mathies
 */
public class IndicesActionProgressMessage extends Message {

    private int percentageCompleted;
    private String message;

    public IndicesActionProgressMessage(){}

    public IndicesActionProgressMessage(int percentageCompleted, String message) {
        this.percentageCompleted = percentageCompleted;
        this.message = message;
    }

    public int getPercentageCompleted() {
        return this.percentageCompleted;
    }

    public String getMessage() {
        return this.message;
    }

}
