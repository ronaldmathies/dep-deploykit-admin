package nl.sodeso.deploykit.admin.client.application.indices.actions;

import nl.sodeso.gwt.websocket.client.Message;

/**
 * @author Ronald Mathies
 */
public class IndicesActionCompletedMessage extends Message {

    private String message;

    public IndicesActionCompletedMessage() {}

    public IndicesActionCompletedMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return this.message;
    }
}
