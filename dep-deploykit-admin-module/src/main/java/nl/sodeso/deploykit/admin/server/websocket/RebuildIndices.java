package nl.sodeso.deploykit.admin.server.websocket;

import nl.sodeso.deploykit.console.domain.Constants;
import nl.sodeso.persistence.elasticsearch.ElasticSearchAdmin;
import nl.sodeso.persistence.elasticsearch.ElasticSearchBulkIndexer;
import nl.sodeso.persistence.elasticsearch.annotation.AnnotationUtil;
import nl.sodeso.persistence.elasticsearch.factory.ElasticSearchClientFactory;
import nl.sodeso.persistence.hibernate.ThreadSafeSession;
import org.elasticsearch.client.Client;
import org.hibernate.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Ronald Mathies
 */
public class RebuildIndices {

    public static void execute(IndicesProgressListener listener, Class[] classesToRebuild, String persistenceUnit, String elasticsearchUnit) {
        Client client = ElasticSearchClientFactory.getInstance().getClient(elasticsearchUnit);

        listener.started(String.format("Started rebuilding '%d' indices.", classesToRebuild.length));

        ProgressTracker tracker = new ProgressTracker(classesToRebuild.length);
        for (Class classToRebuild : classesToRebuild) {
            String index = AnnotationUtil.getElasticSearchIndex(classToRebuild);

            tracker.nextComponent(2);
            tracker.nextStep();
            listener.progress(tracker, String.format("Removing index '%s'", index));
            removeIndex(client, classToRebuild);

            tracker.nextStep();
            listener.progress(tracker, String.format("Finished removing index '%s', started rebuilding index '%s'.", index, index));
            build(classToRebuild, persistenceUnit);
        }

        listener.completed(String.format("Finished rebuilding '%d' indices.", classesToRebuild.length));

    }

    private static void removeIndex(Client client, Class classToRebuild) {
        String index = AnnotationUtil.getElasticSearchIndex(classToRebuild);

        if (ElasticSearchAdmin.indexExists(client, index)) {
            ElasticSearchAdmin.deleteIndex(client, index);
        }
    }

    public static void build(Class classToRebuild, String persistenceUnit) {
        Session session = ThreadSafeSession.getSession(persistenceUnit);

        List<Object> objectsToIndex = new ArrayList<>();

        // Query all the ID's in the database.
        ScrollableResults results = session.createCriteria(classToRebuild)
                .setCacheMode(CacheMode.IGNORE)
                .setFlushMode(FlushMode.MANUAL)
                .setReadOnly(true)
                .scroll(ScrollMode.FORWARD_ONLY);

        // Construct the ID query.
        while (results.next()) {
            objectsToIndex.add(results.get(0));

            if (objectsToIndex.size() == 64 || results.isLast()) {
                ElasticSearchBulkIndexer.bulkIndex(1, Constants.ELASTIC_SEARCH_UNIT, objectsToIndex);
                objectsToIndex.clear();
            }
        }
    }

}
