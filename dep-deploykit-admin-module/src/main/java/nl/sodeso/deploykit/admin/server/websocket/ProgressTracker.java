package nl.sodeso.deploykit.admin.server.websocket;

/**
 * @author Ronald Mathies
 */
public class ProgressTracker {

    private int totalNumberOfComponents;
    private int stepsInComponent;

    private int progress;

    public ProgressTracker(int totalNumberOfComponents) {
        this.totalNumberOfComponents = totalNumberOfComponents;
    }

    public void nextComponent(int stepsInComponent) {
        this.stepsInComponent = stepsInComponent;
    }

    public void nextStep() {
        progress += ((100 / totalNumberOfComponents) / stepsInComponent);
    }

    public int progress() {
        return progress;
    }

}
