package nl.sodeso.deploykit.admin.server.listener;

import nl.sodeso.deploykit.admin.client.Constants;
import nl.sodeso.gwt.ui.server.listener.AbstractModulePropertiesInitializerContextListener;
import nl.sodeso.deploykit.admin.server.endpoint.properties.DeployKitAdminServerModuleProperties;

import javax.servlet.annotation.WebListener;

/**
 * @author Ronald Mathies
 */
@WebListener
public class DeployKitAdminPropertiesInitializerContextListener extends AbstractModulePropertiesInitializerContextListener<DeployKitAdminServerModuleProperties> {

    @Override
    public Class<DeployKitAdminServerModuleProperties> getServerModulePropertiesClass() {
        return DeployKitAdminServerModuleProperties.class;
    }

    @Override
    public String getConfiguration() {
        return "dep-deploykit-admin-configuration.properties";
    }

    @Override
    public String getModuleId() {
        return Constants.MODULE_ID;
    }
}
