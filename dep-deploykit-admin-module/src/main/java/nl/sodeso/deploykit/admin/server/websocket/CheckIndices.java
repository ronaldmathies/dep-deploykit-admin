package nl.sodeso.deploykit.admin.server.websocket;

import nl.sodeso.persistence.elasticsearch.ElasticSearchAdmin;
import nl.sodeso.persistence.elasticsearch.annotation.AnnotationUtil;
import nl.sodeso.persistence.elasticsearch.factory.ElasticSearchClientFactory;
import nl.sodeso.persistence.hibernate.ThreadSafeSession;
import nl.sodeso.persistence.hibernate.criterion.SafeCriterionInClause;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.client.Client;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.IdsQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.hibernate.*;
import org.hibernate.criterion.Projections;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Ronald Mathies
 */
public class CheckIndices {

    public static void execute(IndicesProgressListener listener, Class[] classesToCheck, String persistenceUnit, String elasticsearchUnit) {
        Client client = ElasticSearchClientFactory.getInstance().getClient(elasticsearchUnit);

        listener.started(String.format("Started checking '%d' indices.", classesToCheck.length));

        int numberOfIndicesNeedUpdating = 0;
        ProgressTracker tracker = new ProgressTracker(classesToCheck.length);
        for (int idx = 0; idx < classesToCheck.length; idx++) {
            Class classToIndex = classesToCheck[idx];
            String index = AnnotationUtil.getElasticSearchIndex(classToIndex);

            if (ElasticSearchAdmin.indexExists(elasticsearchUnit, index)) {
                tracker.nextComponent(3);
                tracker.nextStep();

                listener.progress(tracker, String.format("Finding missing documents for index '%s'", index));
                List<Long> idsMissingInElasticSearch = findIdsMissingInElasticSearch(client, index, classToIndex, persistenceUnit);

                tracker.nextStep();
                listener.progress(tracker, String.format("Found '%d' missing documents in the '%s' index, start to check unnecessary documents in the '%s' index.", idsMissingInElasticSearch.size(), index, index));
                List<Long> idsAbsoleteInElasticSearch = findIdsAbsoleteInElasticSearch(client, index, classToIndex, persistenceUnit);

                numberOfIndicesNeedUpdating += !idsMissingInElasticSearch.isEmpty() | !idsAbsoleteInElasticSearch.isEmpty() ? 1 :0;

                tracker.nextStep();
                listener.progress(tracker, String.format("Found '%d' unnecessary documents in the '%s' index.", idsAbsoleteInElasticSearch.size(), index));
            } else {
                tracker.nextComponent(1);
                tracker.nextStep();

                listener.progress(tracker, String.format("Index '%s' does not exist.", index));
            }
        }

        listener.completed(String.format("Finished checking '%d' indices, there are '%d' indices out-of-sync.", classesToCheck.length, numberOfIndicesNeedUpdating) +
                (numberOfIndicesNeedUpdating > 0 ? ", it is advisable to update the indices." : "."));
    }

    private static List<Long> findIdsMissingInElasticSearch(Client client, String index, Class classToIndex, String persistenceUnit) {
        Session session = ThreadSafeSession.getSession(persistenceUnit);

        String type = AnnotationUtil.getElasticSearchType(classToIndex);

        List<Long> missingIds = new ArrayList<>();
        List<String> ids = new ArrayList<>();

        // Query all the ID's in the database.
        ScrollableResults results = session.createCriteria(classToIndex)
                .setProjection(Projections.property("id"))
                .setCacheMode(CacheMode.IGNORE)
                .setFlushMode(FlushMode.MANUAL)
                .setReadOnly(true)
                .scroll(ScrollMode.FORWARD_ONLY);

        // Construct the ID query.
        while (results.next()) {
            ids.add(String.valueOf(results.getLong(0)));

            if (ids.size() == 1000 || results.isLast()) {
                IdsQueryBuilder idsQuery = QueryBuilders.idsQuery(type)
                        .addIds(ids.toArray(new String[ids.size()]));

                // Query the elastic search cluster.
                SearchRequestBuilder searchRequestBuilder = client
                        .prepareSearch(index)
                        .setTypes(type)
                        .setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
                        .addField("_id")
                        .setQuery(idsQuery);
                SearchHits hits = searchRequestBuilder.execute().actionGet().getHits();

                for (SearchHit hit : hits.getHits()) {
                    ids.remove(hit.getId());
                }

                ids.forEach(id -> missingIds.add(Long.parseLong(id)));
                ids.clear();
            }
        }

        System.out.print("Id's missing in ES: " + missingIds);
        return missingIds;
    }

    private static List<Long> findIdsAbsoleteInElasticSearch(Client client, String index, Class classToIndex, String persistenceUnit) {
        Session session = ThreadSafeSession.getSession(persistenceUnit);

        String type = AnnotationUtil.getElasticSearchType(classToIndex);

        List<Long> ids = new ArrayList<>();
        List<Long> absoleteIds = new ArrayList<>();

        SearchResponse searchResponse = client
                .prepareSearch(index)
                .setTypes(type)
                .setSearchType(SearchType.SCAN)
                .setScroll(TimeValue.timeValueSeconds(5))
                .setQuery(QueryBuilders.matchAllQuery())
                .addField("_id")
                .setSize(1000).execute().actionGet();

        while (true) {
            ids.clear();

            SearchHit[] hits = searchResponse.getHits().getHits();
            for (SearchHit hit : hits) {
                ids.add(Long.valueOf(hit.getId()));
            }

            // First time around the search response is empty.
            if (!ids.isEmpty()) {
                ScrollableResults results = session.createCriteria(classToIndex)
                        .add(SafeCriterionInClause.in("id", ids, 256))
                        .setProjection(Projections.property("id"))
                        .setCacheMode(CacheMode.IGNORE)
                        .setFlushMode(FlushMode.MANUAL)
                        .setReadOnly(true)
                        .scroll(ScrollMode.FORWARD_ONLY);

                while (results.next()) {
                    ids.remove(results.getLong(0));
                }

                absoleteIds.addAll(ids);
            }

            searchResponse = client.prepareSearchScroll(searchResponse.getScrollId()).setScroll(TimeValue.timeValueSeconds(5)).execute().actionGet();
            if (searchResponse.getHits().getHits().length == 0) {
                break;
            }
        }

        System.out.print("Id's absolete in ES: " + absoleteIds);
        return absoleteIds;

    }

}
