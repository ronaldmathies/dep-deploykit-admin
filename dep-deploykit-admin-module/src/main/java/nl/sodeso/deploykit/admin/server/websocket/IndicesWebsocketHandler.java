package nl.sodeso.deploykit.admin.server.websocket;

import nl.sodeso.deploykit.admin.client.application.indices.actions.*;
import nl.sodeso.deploykit.console.domain.Constants;
import nl.sodeso.deploykit.console.elasticsearch.IndexedClasses;
import nl.sodeso.gwt.websocket.client.Message;
import nl.sodeso.gwt.websocket.server.WebSocketServerController;
import nl.sodeso.gwt.websocket.server.WebsocketMessageHandler;
import nl.sodeso.persistence.hibernate.TransactionManager;
import nl.sodeso.persistence.hibernate.manager.TransactionReadOnlyResult;
import nl.sodeso.persistence.hibernate.manager.TransactionResult;

import javax.websocket.Session;

/**
 * @author Ronald Mathies
 */
public class IndicesWebsocketHandler implements WebsocketMessageHandler, IndicesProgressListener {

    private static Message lastMessage = null;

    @Override
    public void onMessage(Session session, Message message) {
        if (message instanceof StartCheckIndicesMessage) {
            startCheckIndices();
        } else if (message instanceof StartBuildIndicesMessage) {
            startBuildingIndices();
        } else if (message instanceof StartUpdateIndicesMessage) {
            startUpdatingIndices();
        } else if (message instanceof ResendLastIndicesMessage) {

            if (lastMessage != null) {
                WebSocketServerController.instance().send(session, lastMessage);
            }

        }
    }

    private void startCheckIndices() {
        TransactionManager.executeInReadOnlyTransaction(Constants.PU, () -> {
            CheckIndices.execute(IndicesWebsocketHandler.this, IndexedClasses.getIndexedEntities(), Constants.PU, Constants.ELASTIC_SEARCH_UNIT);
            return new TransactionReadOnlyResult<>(null);
        });
    }

    private void startBuildingIndices() {
        TransactionManager.executeInTransaction(Constants.PU, () -> {
            RebuildIndices.execute(IndicesWebsocketHandler.this, IndexedClasses.getIndexedEntities(), Constants.PU, Constants.ELASTIC_SEARCH_UNIT);
            return new TransactionResult<>(true, null);
        });
    }

    private void startUpdatingIndices() {
        TransactionManager.executeInTransaction(Constants.PU, () -> {
            UpdateIndices.execute(IndicesWebsocketHandler.this, IndexedClasses.getIndexedEntities(), Constants.PU, Constants.ELASTIC_SEARCH_UNIT);
            return new TransactionResult<>(true, null);
        });
    }

    @Override
    public void started(String message) {
        lastMessage = new IndicesActionStartedMessage(message);
        WebSocketServerController.instance().send(lastMessage);
    }

    @Override
    public void progress(ProgressTracker tracker, String message) {
        lastMessage = new IndicesActionProgressMessage(tracker.progress(), message);
        WebSocketServerController.instance().send(lastMessage);
    }

    @Override
    public void completed(String message) {
        lastMessage = new IndicesActionCompletedMessage(message);
        WebSocketServerController.instance().send(lastMessage);
    }
}
