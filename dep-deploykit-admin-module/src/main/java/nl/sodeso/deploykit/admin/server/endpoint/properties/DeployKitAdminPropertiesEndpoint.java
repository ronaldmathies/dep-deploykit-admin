package nl.sodeso.deploykit.admin.server.endpoint.properties;

import nl.sodeso.deploykit.admin.client.Constants;
import nl.sodeso.deploykit.admin.client.properties.DeployKitAdminClientModuleProperties;
import nl.sodeso.gwt.ui.server.endpoint.properties.AbstractModulePropertiesEndpoint;
import nl.sodeso.gwt.ui.server.endpoint.properties.ModulePropertiesContainer;

import javax.servlet.annotation.WebServlet;

/**
 * By default the application has an endpoint for properties consisting of the following settings:
 *
 * <ul>
 *  <li>name: Name of the application (used as the title)</li>
 *  <li>version: Version number of the application.</li>
 *  <li>devkitEnabled: Are the extra development tools available.</li>
 * </ul>
 *
 * If you would like to have more global application settings (not user specific), that don't
 * change during a session, then this is the place to add them. Just add the fields below
 * and fill them using the endpoint.
 *
 * @author Ronald Mathies
 */
@WebServlet(urlPatterns = {"*.deploykitadmin-properties"})
public class DeployKitAdminPropertiesEndpoint extends AbstractModulePropertiesEndpoint<DeployKitAdminClientModuleProperties, DeployKitAdminServerModuleProperties> {

    @Override
    public void fillApplicationProperties(DeployKitAdminServerModuleProperties serverAppProperties, DeployKitAdminClientModuleProperties clientAppProperties) {
    }

    @Override
    public String getModuleId() {
        return Constants.MODULE_ID;
    }

    @Override
    public Class getClientModulePropertiesClass() {
        return DeployKitAdminClientModuleProperties.class;
    }

    @Override
    public DeployKitAdminServerModuleProperties getServerModuleProperties() {
        return ModulePropertiesContainer.instance().getModuleProperties(Constants.MODULE_ID);
    }

}

