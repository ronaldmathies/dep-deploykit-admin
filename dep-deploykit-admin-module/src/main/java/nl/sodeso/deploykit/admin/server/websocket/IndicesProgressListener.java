package nl.sodeso.deploykit.admin.server.websocket;

/**
 * @author Ronald Mathies
 */
public interface IndicesProgressListener {

    /**
     * Called when a indices process has been started.
     *
     * @param message the message that accompanies the start of the process.
     */
    void started(String message);

    /**
     * Called when the indices process has made progress.
     *
     * @param tracker the time tracker.
     * @param message the message that accompanies the progress of the process.
     */
    void progress(ProgressTracker tracker, String message);

    /**
     * Called when the indices process has finished.
     *
     * @param message the message that accompanies the completion of the process.
     */
    void completed(String message);

}
