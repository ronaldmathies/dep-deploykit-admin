package nl.sodeso.deploykit.admin.client.application;

import nl.sodeso.deploykit.console.client.application.group.GroupMenuItem;
import nl.sodeso.deploykit.admin.client.application.indices.Indices;
import nl.sodeso.deploykit.console.client.application.security.role.RoleMenuItem;
import nl.sodeso.deploykit.console.client.application.security.user.UserMenuItem;
import nl.sodeso.gwt.ui.client.controllers.center.CenterController;
import nl.sodeso.gwt.ui.client.controllers.menu.MenuController;
import nl.sodeso.gwt.ui.client.controllers.menu.MenuItem;
import nl.sodeso.gwt.ui.client.resources.Icon;

/**
 * @author Ronald Mathies
 */
public class StartMenu {

    private static Indices indexing = null;

    private StartMenu() {}

    public static void init() {
        MenuController.instance().setFullwidth(true);

        MenuController.instance().addMenuItems(
                new MenuItem("mi-security", Icon.Key, "Security", null)
                        .addChildren(
                                new RoleMenuItem(),
                                new UserMenuItem(),
                                new GroupMenuItem()
                        ),
                new MenuItem("mi-search", Icon.Search, "Search", null)
                        .addChildren(
                            new MenuItem("mi-indexes", "Indexes", arguments -> {
                                if (indexing == null) {
                                    indexing = new Indices();
                                }

                                CenterController.instance().setWidget(indexing);
                            })
                        )
                );
    }

}
