package nl.sodeso.deploykit.admin.server.endpoint.properties;

import nl.sodeso.gwt.ui.server.endpoint.properties.ServerModuleProperties;

/**
 * @author Ronald Mathies
 */
public class DeployKitAdminServerModuleProperties extends ServerModuleProperties {

    public DeployKitAdminServerModuleProperties() {
        super();
    }
}
